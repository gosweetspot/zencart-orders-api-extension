<?php

/*
 * GoSweetSpot Shipping Module for Zen Cart, Version 1.5
 *
 * Copyright (c) 2011 Auctane LLC
 * 
 * Modified by Ezequiel Reyno ezequiel.reyno|at|gmail.com
 *
 * Find out more about GoSweetSpot at www.gosweetspot.com.com
 *
 * Released under the GNU General Public License v2
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; under version 2 of the License
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street - Fifth Floor, Boston, MA 02110-1301, USA.
 */
?>
<?php

require('includes/application_top.php');

//set the content type to xml
header('Content-Type: text/xml');

/**
 * Validate if the apache_request_headers functions exist. If not create a version
 * of this one just to catch the headers of the request.
 * This is a copy of the code in a comment in this documentation
 * http://php.net/manual/en/function.apache-request-headers.php
 */
if( !function_exists('apache_request_headers') ) {
	function apache_request_headers() {
		$arh = array();
		$rx_http = '/\AHTTP_/';
		foreach($_SERVER as $key => $val) {
			if( preg_match($rx_http, $key) ) {
				$arh_key = preg_replace($rx_http, '', $key);
				$rx_matches = array();
				// do some nasty string manipulations to restore the original letter case
				// this should work in most cases
				$rx_matches = explode('_', strtolower($arh_key));
				if( count($rx_matches) > 0 and strlen($arh_key) > 2 ) {
					foreach($rx_matches as $ak_key => $ak_val) $rx_matches[$ak_key] = ucfirst($ak_val);
					$arh_key = implode('-', $rx_matches);
				}
				$arh[$arh_key] = $val;
			}
		}
		if(isset($_SERVER['CONTENT_TYPE'])) $arh['Content-Type'] = $_SERVER['CONTENT_TYPE'];
		if(isset($_SERVER['CONTENT_LENGTH'])) $arh['Content-Length'] = $_SERVER['CONTENT_LENGTH'];
		return( $arh );
	}
}

/**
 * function to add noode to the xml
 * 
 * @para $FieldName string
 * @para $Value string
 * @retrurn xml
 */
function AddFieldToXML($FieldName, $Value) {
    $FindStr = "&";
    $NewStr = "&amp;";
    $Result = str_replace($FindStr, $NewStr, $Value);
    echo "\t\t<$FieldName>$Result</$FieldName>\n";
}

/**
 * function to authenticate username and password sent from the GSS application
 * 
 * @author		Ezequiel Reyno ezequiel.reyno|at|gmail.com
 * @history		12/04/2016 - Include base64 decode for Authorization
 * 				19/04/2016 - Add a new way to get the Authorization header for older versions of PHP
 * 
 * @para $db object
 * @retrurn boolean
 */
function userAuthentication($db = null) {
	$headers = apache_request_headers();
	if(isset($headers['Authorization'])){
		$pieces = explode(" ", $headers['Authorization']);
		$up = base64_decode($pieces[1]);
		$pieces = explode(":", $up);
		$un = $pieces[0];
		$pw = $pieces[1];
	}
	else if(isset($_SERVER["PHP_AUTH_USER"]) && isset($_SERVER["PHP_AUTH_PW"])){
		$un = $_SERVER["PHP_AUTH_USER"];
		$pw = $_SERVER["PHP_AUTH_PW"];
	}
	else {
		header('WWW-Authenticate: Basic realm="GoSweetSpot"');
		header('HTTP/1.0 401 Unauthorized');
		echo 'Unauthorized no Authorization Header present';
		exit;
	}

    if (!isset($un) || strlen($un) == 0) {
        header('WWW-Authenticate: Basic realm="GoSweetSpot"');
        header('HTTP/1.0 401 Unauthorized');
        echo 'Unauthorized';
        exit;
    } else {
        $admin_query = "SELECT admin_id, admin_pass FROM " . TABLE_ADMIN . " WHERE admin_name = '" . $un . "'";
        $admin_result = $db->Execute($admin_query);
        //validate the password with the zencart encrypted password
        if (!zen_validate_password($pw, $admin_result->fields['admin_pass'])) {
            header('WWW-Authenticate: Basic realm="GoSweetSpot"');
            header('HTTP/1.0 401 Unauthorized');
            echo 'Unauthorized';
            exit;
        }
    }
}

/**
 * function to convert the start and end date with zend date format
 * 
 * @para $date date
 * @para $reverse boolean
 * @retrurn date
 */
function zen_date_raw2($date, $reverse = false) {
    if ($reverse) {
        return substr($date, 3, 2) . substr($date, 0, 2) . substr($date, 6, 4);
    } else {
        return substr($date, 6, 4) . '-' . substr($date, 0, 2) . '-' . substr($date, 3, 2) . ' ' . substr($date, 11, 2) . '.' . substr($date, 14, 2) . '.' . '00';
    }
}

/**
 * function to convert the ordr end date with date time format
 * 
 * @para $raw_datetime date
 * @retrurn date
 */
function zen_datetime_short2($raw_datetime) {
    if (($raw_datetime == '0001-01-01 00:00:00') || ($raw_datetime == ''))
        return false;

    $year = (int) substr($raw_datetime, 0, 4);
    $month = (int) substr($raw_datetime, 5, 2);
    $day = (int) substr($raw_datetime, 8, 2);
    $hour = (int) substr($raw_datetime, 11, 2);
    $minute = (int) substr($raw_datetime, 14, 2);
    $second = (int) substr($raw_datetime, 17, 2);

    return strftime(DATE_TIME_FORMAT, mktime($hour, $minute, $second, $month, $day, $year));
}

/**
 * function to authenticate username and password sent from the shipstation
 *
 * @author		Ezequiel Reyno ezequiel.reyno|at|gmail.com
 * @history		12/04/2016 
 * 				- Add a modification to include a filter by status
 * 				- Made a modification to the action verifystatus in order to return the list of the current status.
 * 				- Modify the update order to receive the status thata you want to update. Also update the order object.
 *
 * gss_zc.php?action=export&start_date=06/02/2009%2005:30&end_date=11/05/2009%2022:53
 * gss_zc.php?action=verifystatus
 * gss_zc.php?action=update&order_number=ABC123&status=4&comment=commment
 *
 * @para 
 * @retrurn XML with the order
 */
if ($_GET['action'] == 'export') {
    //call the user authentication before start of order data replication
    userAuthentication($db);

    $sd = zen_date_raw2((!isset($_GET['start_date']) ? date("m-d-Y", (time())) : $_GET['start_date']));
    $ed = zen_date_raw2((!isset($_GET['end_date']) ? date("m-d-Y", (time())) : $_GET['end_date']));
    
    $arr_status = explode(",", !isset($_GET['statuses']) ? "" : $_GET['statuses']);
    
    //get order from database
    $orders_query = "SELECT * FROM " . TABLE_ORDERS . " WHERE orders_id >'0' and IFNULL(last_modified, date_purchased) BETWEEN '" . $sd . "' AND DATE_ADD('" . $ed . "', INTERVAL 1 MINUTE)";

    //Add filter by status.
    if(!isset($arr_status) && $arr_status[0] != ""){
    	$orders_query .= "AND ( 1=1 ";
	    foreach ($arr_status as $value) {
		    $orders_query .= "OR orders_status = " .$value;
		}
		$orders_query .= " )";
    }
    $orders_result = $db->Execute($orders_query);

    if ($orders_result->fields['orders_id']) {
        //begin outputing XML
        echo "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
        echo "<Orders>\n";
        //process orders
        while (!$orders_result->EOF) {
            //get order items from datbase
            $orderitems_query = "SELECT * FROM " . TABLE_ORDERS_PRODUCTS . " WHERE orders_id = '" . $orders_result->fields['orders_id'] . "'";
            $orderitems_result = $db->Execute($orderitems_query);

            $cust_id = $orders_result->fields['customers_id'];
            $cust_deff_id = $orders_result->fields['customers_default_address_id'];
            $address_query = "SELECT * FROM " . TABLE_ADDRESS_BOOK . " WHERE customers_id = '$cust_id' and address_book_id = '$cust_deff_id'";
            $address_result = $db->Execute($address_query);

            //billing country code
            $billing_country_id = $orders_result->fields['billing_country'];
            $billing_query = "SELECT * FROM " . TABLE_COUNTRIES . " WHERE countries_name = '$billing_country_id'";
            $billing_result = $db->Execute($billing_query);
            //billing country code
            //shipping country code
            $shipping_country_id = $orders_result->fields['delivery_country'];
            $shipping_query = "SELECT * FROM " . TABLE_COUNTRIES . " WHERE countries_name = '$shipping_country_id'";
            $shipping_result = $db->Execute($shipping_query);
            //billing zone code
            $billing_zone_id = addslashes($orders_result->fields['billing_state']);
            $zone_billing_query = "SELECT * FROM " . TABLE_ZONES . " WHERE zone_name = '$billing_zone_id'";
            $zone_billing_result = $db->Execute($zone_billing_query);
            //billing zone code
            //shipping zone code
            $shipping_zone_id = addslashes($orders_result->fields['delivery_state']);
            $zone_shipping_query = "SELECT * FROM " . TABLE_ZONES . " WHERE zone_name = '$shipping_zone_id'";
            $zone_shipping_result = $db->Execute($zone_shipping_query);
            //shipping zone code

            $ship_order_id = $orders_result->fields['orders_id'];
            $ship_query = "SELECT * FROM " . TABLE_ORDERS_TOTAL . " WHERE orders_id = '$ship_order_id' and class = 'ot_shipping'";
            $ship_result = $db->Execute($ship_query);

            $orders_status = $db->Execute("select orders_status_id, orders_status_name
	                                 from " . TABLE_ORDERS_STATUS . "
	                                 where language_id = '" . (int) $_SESSION['languages_id'] . "' and orders_status_id = '" . $orders_result->fields['orders_status'] . "'");

            if ($orders_status->fields['orders_status_name']) {
                $order_status = $orders_status->fields['orders_status_name'];
                $order_status_id = $orders_status->fields['orders_status_id'];
            } else {
                $order_status = '';
                $order_status_id = '';
            }

            if ($order_status_id) {
                $orders_history = $db->Execute("select date_added
	                                    from " . TABLE_ORDERS_STATUS_HISTORY . "
	                                    where orders_id = '" . zen_db_input($orders_result->fields['orders_id']) . "' and orders_status_id = '" . $order_status_id . "'
	                                    order by date_added LIMIT 1");

                $last_modified = zen_datetime_short2($orders_history->fields['date_added']);

                $orders_history_comments = $db->Execute("select comments
	                                      from " . TABLE_ORDERS_STATUS_HISTORY . "
	                                      where orders_id = '" . zen_db_input($orders_result->fields['orders_id']) . "' and (comments is not null or comments != '')
	                                    order by date_added LIMIT 1");

                $shipping_comments = $orders_history_comments->fields['comments'];
            } else {
                $last_modified = '';
                $shipping_comments = '';
            }

            echo "\t<Order>\n";

            //order details
            AddFieldToXML("OrderNumber", $orders_result->fields['orders_id']);
            AddFieldToXML("OrderDate", zen_datetime_short2($orders_result->fields['date_purchased']));
            AddFieldToXML("OrderStatusCode", $order_status_id);
            AddFieldToXML("OrderStatusName", $order_status);
            AddFieldToXML("LastModified", $last_modified);
            AddFieldToXML("LastModifiedOrderTable", $orders_result->fields['last_modified']);
            AddFieldToXML("date_purchased", $orders_result->fields['date_purchased']);
            AddFieldToXML("PaymentMethod", '<![CDATA[' . $orders_result->fields['payment_method'] . ']]>');
            AddFieldToXML("PaymentMethodCode", $orders_result->fields['payment_module_code']);
            AddFieldToXML("ShippingMethod", '<![CDATA[' . $orders_result->fields['shipping_method'] . ']]>');
            //AddFieldToXML("ShippingMethodCode", $orders_result->fields['shipping_module_code']);
            AddFieldToXML("ShippingMethodCode", '<![CDATA[' . $orders_result->fields['shipping_method'] . ']]>');
            AddFieldToXML("CouponCode", $orders_result->fields['coupon_code']);
            AddFieldToXML("Currency", $orders_result->fields['currency']);
            AddFieldToXML("CurrencyValue", $orders_result->fields['currency_value']);
            AddFieldToXML("OrderTotal", $orders_result->fields['order_total']);
            AddFieldToXML("TaxAmount", $orders_result->fields['order_tax']);
            AddFieldToXML("ShippingAmount", $ship_result->fields['value']);
            AddFieldToXML("CommentsFromBuyer", '<![CDATA[' . $shipping_comments . ']]>');
            //order details
            //customer details
            echo "\t<Customer>\n";

            AddFieldToXML("CustomerNumber", $orders_result->fields['customers_id']);

            //billing details
            echo "\t<BillTo>\n";

            $billing_state = $orders_result->fields['billing_state'];

            AddFieldToXML("Name", '<![CDATA[' . $orders_result->fields['billing_name'] . ']]>');
            AddFieldToXML("Company", '<![CDATA[' . $orders_result->fields['billing_company'] . ']]>');
            AddFieldToXML("Address1", '<![CDATA[' . $orders_result->fields['billing_street_address'] . ']]>');
            AddFieldToXML("Address2", '<![CDATA[' . $orders_result->fields['billing_suburb'] . ']]>');
            AddFieldToXML("City", '<![CDATA[' . $orders_result->fields['billing_city'] . ']]>');
            AddFieldToXML("State", '<![CDATA[' . $billing_state . ']]>');
            AddFieldToXML("StateCode", $zone_billing_result->fields['zone_code']);
            AddFieldToXML("PostalCode", $orders_result->fields['billing_postcode']);
            AddFieldToXML("Country", $orders_result->fields['billing_country']);
            AddFieldToXML("CountryCode", $billing_result->fields['countries_iso_code_2']);
            AddFieldToXML("Phone", $orders_result->fields['customers_telephone']);
            AddFieldToXML("Email", $orders_result->fields['customers_email_address']);
            //	AddFieldToXML("CountryCode", $country_result->fields['countries_iso_code_2']);

            echo "\t</BillTo>\n";
            //billing details
            //shipping details

            echo "\t<ShipTo>\n";

            $shipping_state = $orders_result->fields['delivery_state'];

            AddFieldToXML("Name", '<![CDATA[' . $orders_result->fields['delivery_name'] . ']]>');
            AddFieldToXML("Company", '<![CDATA[' . $orders_result->fields['delivery_company'] . ']]>');
            AddFieldToXML("Address1", '<![CDATA[' . $orders_result->fields['delivery_street_address'] . ']]>');
            AddFieldToXML("Address2", '<![CDATA[' . $orders_result->fields['delivery_suburb'] . ']]>');
            AddFieldToXML("City", '<![CDATA[' . $orders_result->fields['delivery_city'] . ']]>');
            AddFieldToXML("State", '<![CDATA[' . $shipping_state . ']]>');
            AddFieldToXML("StateCode", $zone_shipping_result->fields['zone_code']);
            AddFieldToXML("PostalCode", $orders_result->fields['delivery_postcode']);
            AddFieldToXML("Country", $orders_result->fields['delivery_country']);
            AddFieldToXML("CountryCode", $shipping_result->fields['countries_iso_code_2']);

            echo "\t</ShipTo>\n";
            //shipping details

            echo "\t</Customer>\n";
            //customer details
            echo "\t<Items>\n";
            //process Order Items
            while (!$orderitems_result->EOF) {

                $image_query = "SELECT products_image, products_weight FROM " . TABLE_PRODUCTS . " WHERE products_id = '" . $orderitems_result->fields['products_id'] . "'";
                $image_result = $db->Execute($image_query);

                echo "\t<Item>\n";
                AddFieldToXML("ProductID", $orderitems_result->fields['products_id']);
                AddFieldToXML("SKU", '<![CDATA[' . $orderitems_result->fields['products_model'] . ']]>');
                AddFieldToXML("Name", '<![CDATA[' . $orderitems_result->fields['products_name'] . ']]>');
                AddFieldToXML("ImageUrl", HTTP_SERVER . DIR_WS_CATALOG . DIR_WS_IMAGES . $image_result->fields['products_image']);
                AddFieldToXML("Weight", $image_result->fields['products_weight']);
                AddFieldToXML("UnitPrice", round($orderitems_result->fields['final_price'], 2));
                AddFieldToXML("TaxAmount", round($orderitems_result->fields['products_tax'], 2));
                AddFieldToXML("Quantity", $orderitems_result->fields['products_quantity']);

                $orderitems_attributes_query = "SELECT orders_products_attributes_id, products_options, products_options_values  FROM " . TABLE_ORDERS_PRODUCTS_ATTRIBUTES . " WHERE orders_id = '" . $orders_result->fields['orders_id'] . "' and orders_products_id = '" . $orderitems_result->fields['orders_products_id'] . "'";
                $orderitems_attributes_result = $db->Execute($orderitems_attributes_query);

                if ($orderitems_attributes_result->fields['orders_products_attributes_id']) {
                    echo "\t<Options>\n";
                }

                while (!$orderitems_attributes_result->EOF) {

                    echo "\t<Option><Name><![CDATA[" . $orderitems_attributes_result->fields['products_options'] . "]]></Name><Value><![CDATA[" . $orderitems_attributes_result->fields['products_options_values'] . "]]></Value></Option>\n";

                    $orderitems_attributes_result->MoveNext();
                }

                if ($orderitems_attributes_result->fields['orders_products_attributes_id']) {
                    echo "\t</Options>\n";
                }

                echo "\t</Item>\n";

                $orderitems_result->MoveNext();
            }
            //process Order Items
            echo "\t</Items>\n";
            echo "\t</Order>\n";

            $orders_result->MoveNext();
        }

        //process Orders
        //finish outputing XML
        echo "</Orders>";
    } else {

        echo "<?xml version=\"1.0\" encoding=\"utf-16\"?>\n";
        echo "<Orders />\n";
    }
} elseif ($_GET['action'] == 'verifystatus') {
	userAuthentication($db);

	$status_query .= "select orders_status_id, orders_status_name from " . TABLE_ORDERS_STATUS . " where language_id = '";
	$status_query .= (int)$_SESSION['languages_id'] . "'";
	$orders_status = $db->Execute($status_query);
	
	if ($orders_status->fields['orders_status_id']) {
		header('Content-type: application/xml');
		//begin outputing XML
		echo "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
		echo "<Statuses>\n";
		while (!$orders_status->EOF) {
			if ($orders_status->fields['orders_status_name']) {
				echo "\t<Status>\n";
				AddFieldToXML("OrderStatusCode", $orders_status->fields['orders_status_id']);
				AddFieldToXML("OrderStatusName", $orders_status->fields['orders_status_name']);
				echo "\t</Status>\n";
			}
			$orders_status->MoveNext();
		}
		echo "</Statuses>\n";
	}	
	else {
		header('Content-type: application/xml');
		echo "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
		echo "<Statuses />\n";
	}
} elseif ($_GET['action'] == 'update') {
    userAuthentication($db);
    header('Content-type: text/plain');
    if ($_GET['order_number']) {

        $status = $_GET['status'];
        $customer_notified = '0';
        $comments = $_GET['comment'];


        $record_query = "SELECT orders_id FROM " . TABLE_ORDERS . " WHERE orders_id = '" . $_GET['order_number'] . "'";
        $record_result = $db->Execute($record_query);

        if ($record_result->fields['orders_id']) {
        	
        	$update_query = "UPDATE " . TABLE_ORDERS ." SET `last_modified` = now(),`orders_status` =" . zen_db_input($status) 
        				." WHERE `orders_id` = " . $_GET['order_number'] .";";

        	$db->Execute($update_query);

			$db->Execute("insert into " . TABLE_ORDERS_STATUS_HISTORY . "
	        			(orders_id, orders_status_id, date_added, customer_notified, comments)
                    	values ('" .(int)$_GET['order_number'] . "','" . zen_db_input($status) 
						."', now(), '" . zen_db_input($customer_notified) . "','" . zen_db_input("http://gosweetspot.co.nz/track/" .$comments) . "')");
			echo 'OK-Status updated successfully';
        } else {
            echo 'KO-Order does not exist in database';
        }
    } else {
        echo 'KO-No order number';
    }
} else {
    echo 'KO-No action parameter. Please contact software provider.';
}
?>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>

